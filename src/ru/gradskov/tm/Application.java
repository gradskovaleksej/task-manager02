package ru.gradskov.tm;

import ru.gradskov.tm.constants.TerminalConst;

public class Application implements TerminalConst {

    public static void main(String[] args) {
        printWelcome();
        run(args);

    }

    private static void printWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String arg = args[0];
        if (arg.isEmpty()) return;
        switch (arg.trim().toLowerCase()) {
            case ABOUT:
                printAbout();
                break;
            case HELP:
                printHelp();
                break;
            case VERSION:
                printVersion();
                break;
        }
    }

    private static void printHelp() {
        System.out.println("[HELP]");
        System.out.println(VERSION + " - Display program version.");
        System.out.println(ABOUT + " - Display developer info.");
        System.out.println(HELP + " - Display list of terminal commands.");
        System.exit(0);
    }

    private static void printVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.3");
        System.exit(0);
    }

    private static void printAbout() {
        System.out.println("[ABOUT]");
        System.out.println("A. Gradskov");
        System.out.println("gradskov1546@yandex.ru");
        System.exit(0);
    }

}
