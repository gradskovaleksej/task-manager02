package ru.gradskov.tm.constants;

public interface TerminalConst {

    String VERSION = "version";
    String ABOUT = "about";
    String HELP = "help";

}
